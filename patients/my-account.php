<?php
// Start Session
session_start();
// check user login
if(empty($_SESSION['id']))
{
    header("Location: ../login.php");
}
// Database connection
require __DIR__ . '../../lib/connect.php';
$db = DB();
// Application library ( with DemoLib class )
require __DIR__ . '../../lib/library.php';
$app = new CmsAdminLib();
$user = $app->UserDetails($_SESSION['id']); // get user details

?>
<?php 

$query = "SELECT * FROM `patients`";
include '../header.php';  ?>

    <body>

        <div id="layout">

            <?php// include 'header-nav.php';  ?>
            <header>
                <div class="container">
                    <div class="row">
                     
                    <div class="logo inner-logo">
                        <a href="../index.php"><img src="images/logo-white.png" alt="logo" class="img-responsive"></a>
                    </div>
                     
                     
                    <div class="tools-top">
                     
                        <div class="avatar-profile">
                            <div class="user-edit">
                                <h4><strong><?php echo $user->first_name ;?> <?php echo $user->last_name ;?></strong></h4>
                                <a href="my-account.php">edit profile</a>
                            </div>
                            <div class="avatar-image">

                                <img width="38px" height="38px" src="../admin/images/<?php echo $user->image; ?>"/>
                                <!-- <a href="appointments-reserved.php" title="2 Notifications Pending">
                                <span class="notifications" style="display: inline;">2</span>
                                </a> -->
                            </div>
                        </div>
                         
                        <ul class="tools-help">
                            <li><a href="p_help.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question-circle"></i></a></li>
                            <li><a href="../logout.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Logout"><i class="fa fa-sign-out"></i></a></li>
                        </ul>
                    </div>
                     
                    </div>
                </div>
            </header>
             
            <nav>
                <div class="container">
                    <h4 class="navbar-brand">menu</h4>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav main-menu">
                            <li class="active"> <a href="my-account.php">my account</a> </li>
                            <li> <a href="booked-calendar.php">book an appointment</a> </li>
                            <li> <a href="appointments-reserved.php">appointments reserved</a> </li>
                            <li> <a href="patients-examinations.php">Result Examinations</a> </li>
                        </ul>
                        </div>
                    </div>
                </div>
            </nav>

            <div class="container">
               <?php /*
                <div class="main-container">
                    <!--Form-->
                    <div class="register-form edit-account">

                        <div class="form-login">
                            <form>
                                <!--Personal Information-->
                                <div class="row">

                                    <h3>Personal Information</h3>
                                    <div class="datapos">

                                        <!--select a document-->
                                        <div class="icon-data">
                                            <i class="fa fa-address-card"></i>
                                        </div>
                                        <input name="id" value="<?php echo $user->id ?>" type="text" disabled>
                                        <!--select a document-->

                                        <!--name-->
                                        <div class="icon-data">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input type="text" name="first_name" value="<?php echo $user->first_name ?>">
                                        <!--name-->

                                        <!--phone-->
                                        <div class="icon-data">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <input type="text" name="contact_no" value="<?php echo $user->contact_no ?>">
                                        <!--phone-->
                                        <!--gender-->
                                        <label>
                                            <input type="radio" name="gender" value="value="Female"<?php if($user->gender == 'Female'): ?> checked="checked"<?php endif; ?>"> Female
                                        </label>
                                        <label>
                                            <input type="radio" name="gender" value="value="Male"<?php if($user->gender == 'Male'): ?> checked="checked"<?php endif; ?>"> Male
                                        </label>

                                        <!--gender-->
                                    </div>

                                    <div class="datapos">

                                         <!--Alternative phone-->
                                        <div class="icon-data">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                        <input type="text" name="email" value="<?php echo $user->email ?>">
                                        <!--Alternative phone-->
                                        <!--Last name-->
                                        <div class="icon-data">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input type="text" name="last_name" value="<?php echo $user->last_name ?>">
                                        <!--Last name-->
                                       
                                        <div class="icon-data">
                                            <i class="fa fa-user-circle"></i>
                                        </div>
                                        <input type="text" name="password" value="<?php echo $user->password ?>">
                                        <!--Alternative phone-->

                                         <!--Avatar Profile-->
                                        <div class="avatar-profile">
                                            <label>Image Profile</label>
                                            
                                            <img  src="../admin/images/<?php echo $user->image ?>" alt="" class="img-thumbnail profile-image">
                                        </div>
                                    </div>

                                </div>
                                <!--Personal Information-->
                                <button class="btn btn-default">save changes</button>

                                <span class="help">
                                    <a href="my-account.php#" class="help-link">Help?</a>
                                </span>
                            </form>
                        </div>
                    </div>
                    <!--Form-->
                </div>
*/ ?>
               

                <div class="row" style="margin-top: 40px;">
                  <div class="col-md-4">
                    <img  src="../admin/images/<?php echo $user->image ?>" alt="" class="img-thumbnail profile-image">
                  </div>
                  <div class="col-md-8">
                    
                    <div class="pro-desc">
                     <?php /* <h3><strong><?php echo $user->first_name." ".$user->last_name ?></strong></h3> */ ?>

                      <table class="table table-striped">
                        <tbody>
                         <tr>
                          <td><strong>ID Number:</strong></td>
                          <td><?php echo $user->id ?></td>
                        </tr>
                        <tr>
                          <td><strong>First Name:</strong></td>
                          <td><?php echo $user->first_name ?></a></td>
                        </tr>
                        <tr>
                          <td><strong>Last Name:</strong></td>
                          <td><?php echo $user->last_name ?></td>
                        </tr>
                        <tr>
                          <td><strong>Email:</strong></td>
                          <td><?php echo $user->email ?></a></td>
                        </tr>
                        <tr>
                          <td><strong>Username</strong></td>
                          <td><?php echo $user->username ?></td>
                        </tr>
                        <tr>
                          <td><strong>Password</strong></td>
                          <td><?php echo $user->password ?></td>
                        </tr>
                        <tr>
                          <td><strong>Contact No:</strong></td>
                          <td><?php echo $user->contact_no ?></td>
                        </tr>
                        <tr>
                          <td><strong>Gender:</strong></td>
                          <td><?php echo $user->gender ?></td>
                        </tr>
                        <?php /*
                        <tr>
                          <td><strong>Created Date</strong></td>
                          <td><?php echo date("d/m/Y", strtotime($patients['created_at']));?></td>
                        </tr>
                        <tr>
                          <td><strong>Modification Date</strong></td>
                          <td><?php echo date("d/m/Y", strtotime($patients['modified_at']));?></td>
                        </tr>
                        */?>
                        </tbody>
                      </table>
                    </div>
                  </div>
              </div>

            </div>

    <?php include '../footer.php';  ?>