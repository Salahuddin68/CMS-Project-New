<?php
// Start Session
session_start();
// check user login
if(empty($_SESSION['id']))
{
    header("Location: ../login.php");
}
// Database connection
require __DIR__ . '../../lib/connect.php';
$db = DB();
// Application library ( with DemoLib class )
require __DIR__ . '../../lib/library.php';
$app = new CmsAdminLib();
$user = $app->UserDetails($_SESSION['id']); // get user details

?>
<?php include '../header.php';  ?>
<body>
<div id="layout">

<?php// include 'header-nav.php';  ?>
<header>
	<div class="container">
		<div class="row">
		 
		<div class="logo inner-logo">
			<a href="../index.php"><img src="images/logo-white.png" alt="logo" class="img-responsive"></a>
		</div>
		 
		 
		<div class="tools-top">
		 
			<div class="avatar-profile">
				<div class="user-edit">
					<h4><strong><?php echo $user->first_name ;?> <?php echo $user->last_name ;?></strong></h4>
					<a href="my-account.php">edit profile</a>
				</div>
				<div class="avatar-image">

					<img width="38px" height="38px" src="../admin/images/<?php echo $user->image; ?>"/>
					<!-- <a href="appointments-reserved.php" title="2 Notifications Pending">
					<span class="notifications" style="display: inline;">2</span>
					</a> -->
				</div>
			</div>
			 
			<ul class="tools-help">
				<li><a href="p_help.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question-circle"></i></a></li>
				<li><a href="../logout.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Logout"><i class="fa fa-sign-out"></i></a></li>
			</ul>
		</div>
		 
		</div>
	</div>
</header>
 
<nav>
	<div class="container">
		<h4 class="navbar-brand">menu</h4>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<i class="fa fa-bars" aria-hidden="true"></i>
			</button>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav main-menu">
				<li> <a href="my-account.php">my account</a> </li>
                <li> <a href="booked-calendar.php">book an appointment</a> </li>
                <li class="active"> <a href="appointments-reserved.php">appointments reserved</a> </li>
                <li> <a href="patients-examinations.php">Result Examinations</a> </li>
			</ul>
			
			</div>
		</div>
	</div>
</nav>
 
<section class="container">
	<div class="main-container">
	<div class="row">
	<div class="listed">
		<div class="row">
		<!-- <div class="filters">
			<h4>Filter by</h4>
			<ul class="list-unstyled">
				<li><a href="appointments-reserved.php" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="highest to lowest"><i class="fa fa-arrow-down" aria-hidden="true"></i></a></li>
				<li><a href="appointments-reserved.php" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="lowest to highest"><i class="fa fa-arrow-up" aria-hidden="true"></i></a></li>
				<li>
				<select>
				<option>Type of Appointment</option>
				<option>General</option>
				<option>Specialist</option>
				</select>
				</li>
			</ul>
		</div> -->
		</div>
	<div class="row">
	 
	<div class="col-lg-12">
		<div class="item-meeting">
		 
			<div class="avatar-doctor">
				<div class="avatar-image">
					<img src="images/doctor12.jpg" alt="doctor" class="img-responsive">
					<i class="fa fa-user-md" aria-hidden="true"></i>
					<a href="meet-doctors.php" title="See Profile">Prof. (Dr.) M. A. Samad</a>
					<p>Nephrology</p>
				</div>
			</div>
			<div class="data-meeting">
				<ul class="list-unstyled info-meet">
					<li><p>no.ticket: <span>9876005-00</span></p></li>
					<li><p class="time">Datetime: <span>September 15, 2017 at 10:00am</span></p></li>
					<li><p>type of appointment: <span>General Medical Assistance</span></p></li>
					<li><p>total: <span class="price">1500.00</span></p></li>
					<li><p>doctor's office: <span>202</span></p></li>
					<!-- <li><div class="alert alert-info" role="alert">Don't forget the copy of identification number.</div></li> -->
				</ul>
				<ul class="list-unstyled btns">
					<li><a class="btn btn-xsmall" href="appointments-reserved.php#"><i class="fa fa-arrow-down" aria-hidden="true"></i> print</a></li>
					<li><a class="btn btn-green btn-xsmall" href="modify-booked-calendar.php"><i class="fa fa-pencil" aria-hidden="true"></i> modify</a></li>
					<li><button class="btn btn-red btn-xsmall confirm"><i class="fa fa-times" aria-hidden="true"></i> cancel</button></li>
				</ul>
			</div>
		</div>
	</div>
	 
	</div>
	<div class="row">
	 
	<div class="col-lg-12">
		<div class="item-meeting">
		 
			<div class="avatar-doctor">
				<div class="avatar-image">
					<img src="images/doctor13.jpg" alt="doctor" class="img-responsive">
					<i class="fa fa-user-md" aria-hidden="true"></i>
					<a href="meet-doctors.php" title="See Profile">Dr. Amjad Hossain</a>
					<p>Orthopedic & Trauma</p>
				</div>
			</div>
			<div class="data-meeting">
				<ul class="list-unstyled info-meet">
					<li><p>no.ticket: <span>9876005-00</span></p></li>
					<li><p class="time">Datetime: <span>December 30, 2017 at 08:00am</span></p></li>
					<li><p>type of appointment: <span>Specialist</span></p></li>
					<li><p>total: <span class="price">1250.00</span></p></li>
					<li><p>doctor's office: <span>202</span></p></li>
					<!-- <li><p>Observations: None.</p></li> -->
				</ul>
				<ul class="list-unstyled btns">
					<li><a class="btn btn-xsmall" href="appointments-reserved.php#"><i class="fa fa-arrow-down" aria-hidden="true"></i> print</a></li>
					<li><a class="btn btn-green btn-xsmall" href="/modify-booked-calendar.php">
					<i class="fa fa-pencil" aria-hidden="true"></i> modify</a></li>
					<li><button class="btn btn-red btn-xsmall confirm"><i class="fa fa-times" aria-hidden="true"></i> cancel</button></li>
				</ul>
			</div>
		</div>
	</div>
	 
	</div>
	<div class="row">
		<div class="load-more">
			<a class="btn btn-green btn-small" href="appointments-reserved.php#"> Load more</a>
		</div>
	</div>
	</div>
	 
	<aside>
		<div class="elements-aside">
			<ul>
				<li class="color-1">
					<i class="fa fa-heartbeat" aria-hidden="true"></i>
					<h4>Emergency Case</h4>
					<p>If you need a doctor urgently outside of medicenter opening hours, call emergency appointment number for emergency service.</p>
				</li>
				<li class="color-2">
					<i class="fa fa-hourglass-half" aria-hidden="true"></i>
					<h4>Working Time</h4>
					<p>Monday to Friday <span> 09:00am to 05:00pm</span></p>
					<p>Weekends <span> 09:00am to 12:00pm</span></p>
				</li>
				<li class="color-1">
					<i class="fa fa-id-badge" aria-hidden="true"></i>
					<h4>Medical Services</h4>
					<p>If you need a doctor urgently outside of medicenter opening hours, call emergency appointment number for emergency service.</p>
				</li>
				<li class="color-3">
					<i class="fa fa-info" aria-hidden="true"></i>
					<h4>Doubts?</h4>
					<p>Office 8/2A, Katasur, Mohammadpur <span>Dhaka-1207</span></p>
				</li>
			</ul>
		</div>
	</aside>
	 
	</div>
	</div>
</section>

<?php include '../footer.php';  ?>