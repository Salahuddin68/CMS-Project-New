<?php
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `reports`";
// var_dump($query);
include 'header.php'; 
?>

<!-- <?php include 'header.php';  ?> -->

    <body>

        <div id="layout">

            <header>
                <div class="container">
                    <div class="row">
                        <!--Logo-->
                        <div class="logo inner-logo">
                            <a href="index.php"><img src="images/logo-white.png" alt="logo" class="img-responsive"></a>
                        </div>
                        <!--Logo-->

                        <!--Header tools-->
                        <div class="tools-top">
                            <!--Avatar-->
                           <!--  <div class="avatar-profile">
                                <div class="user-edit">
                                    <h4>Jalal Ahamad</h4>
                                    <a href="my-account.php">edit profile</a>
                                </div>
                                <div class="avatar-image">
                                    <img src="images/avatar-profile.jpg" alt="avatar profile" class="img-responsive">
                                    <a href="appointments-reserved.php" title="2 Notifications Pending">
                                        <span class="notifications" style="display: inline;">2</span>
                                    </a>
                                </div>
                            </div> -->
                            <!--Avatar-->

                           <!--  <ul class="tools-help">
                                <li><a href="help.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question-circle"></i></a></li>
                                <li><a href="login.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Logout"><i class="fa fa-sign-out"></i></a></li>
                            </ul> -->
                        </div>
                        <!--Header tools-->
                    </div>
                </div>
            </header>

            <!--Menu-->
            <nav>
                <div class="container">
                    <h4 class="navbar-brand">menu</h4>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                          <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse">

                        <ul class="nav navbar-nav">
                            <li> <a href="index.php">Home</a> </li>
                            <!-- <li> <a href="appointments-reserved.php">appointments reserved</a> </li> -->
                            <!-- <li> <a href="booked-calendar.php">book an appointment</a> </li> -->
                            <li> <a href="login.php">book an appointment</a> </li>
                            <li class="active"> <a href="examinations.php">Result Examinations</a> </li>
                            <!-- <li> <a href="my-account.php">my account</a> </li> -->
                        </ul>
                      
                    </div>
                </div>
            </nav>
            <!--Menu-->

            <section class="container">
                <div class="main-container">
                    <div class="row">
                        <div class="listed">
                            <div class="row">
                                <!-- <div class="filters">
                                <h4>Filter by</h4>
                                    <ul class="list-unstyled">
                                        <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="highest to lowest"><i class="fa fa-arrow-down" aria-hidden="true"></i></a></li>
                                        <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="lowest to highest"><i class="fa fa-arrow-up" aria-hidden="true"></i></a></li>
                                        <li>
                                            <select>
                                                <option selected="">Month</option>
                                                <option value="1">Janaury</option>
                                                <option value="2">February</option>
                                                <option value="3">March</option>
                                                <option value="4">April</option>
                                                <option value="5">May</option>
                                                <option value="6">June</option>
                                                <option value="7">July</option>
                                                <option value="8">August</option>
                                                <option value="9">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                        </li>
                                    </ul>
                                </div> -->
                            </div>

                            <!--Monthly list-->
                            <div class="row">
                                <div class="box-listed">
                                    
                                    <?php 
                                            foreach($db->query($query) as $reports) { ?>
                                    <ul class="list-unstyled box-item-list">
                                    
                                        <li>
                                            <i class="fa fa-file-text-o type-icon"></i>
                                            <span><?php echo date("d M Y", strtotime($reports['created_at'])); ?></span>
                                            <span class="type-test"><?php echo $reports['report_title'];?></span>
                                            <span class="download-link"><a href="admin/images/<?php echo $reports["report_file"]; ?>" download="" class="hvr-icon-down">Download</a></span>
                                        </li>
                                    </ul><?php }?>
                                </div>

                               

                                
                            </div>

                            <div class="row">
                                <div class="load-more">
                                    <a class="btn btn-green btn-small" href="#"> Load more</a>
                                </div>
                            </div>
                        </div>

                        <!--Aside-->
                        <aside>
                            <div class="elements-aside solid-color">
                                <ul>
                                    <li class="color-1">
                                        <i class="fa fa-heartbeat" aria-hidden="true"></i>
                                        <h4>Emergency Case</h4>
                                        <p>If you need a doctor urgently outside of medicenter opening hours, call emergency appointment number for emergency service.</p>
                                    </li>
                                    <li class="color-2">
                                        <i class="fa fa-hourglass-half" aria-hidden="true"></i>
                                        <h4>Working Time</h4>
                                        <p>Monday to Friday <span> 09:00am to 05:00pm</span></p>
                                        <p>Weekends <span> 09:00am to 12:00pm</span></p>
                                    </li>
                                    <li class="color-3">
                                        <i class="fa fa-id-badge" aria-hidden="true"></i>
                                        <h4>Medical Services</h4>
                                        <p>If you need a doctor urgently outside of medicenter opening hours, call emergency appointment number for emergency service.</p>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                        <!--Aside-->
                    </div>
                </div>
            </section>

        </div>

    <?php include 'footer.php';  ?>

