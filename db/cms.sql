-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2017 at 08:26 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `id` int(11) NOT NULL,
  `doctors` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `type_of_appoinment` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `appoinment_time` time NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `specialist` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `available_date` date NOT NULL,
  `time` varchar(255) NOT NULL,
  `office` varchar(255) NOT NULL,
  `biodata` text NOT NULL,
  `picture` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `name`, `specialist`, `department`, `available_date`, `time`, `office`, `biodata`, `picture`, `created_at`, `modified_at`) VALUES
(5, 'Asia Khanam', 'NEPHROLOGY', 'Health Care', '2017-05-25', '11am-12pm', '202', '', 'doctor13.jpg', '2017-05-20 18:44:00', '2017-05-29 00:22:49'),
(6, 'dsaf', 'safd', 'dsfa', '2017-05-26', '3pm-5pm', '204', '', 'doctor15.jpg', '2017-05-20 19:05:06', '2017-05-29 00:22:59'),
(7, 'dsf', 'sdf', 'safds', '2017-05-29', '11am-12pm', '203', '<p><strong>safdsa</strong></p>', 'doctor17.jpg', '2017-05-20 19:06:26', '2017-05-29 00:23:31');

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE `fees` (
  `id` int(11) NOT NULL,
  `genaral_fee` int(11) NOT NULL,
  `specialist_fee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `confirm_password` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `contact_no` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `confirm_password`, `gender`, `image`, `contact_no`, `created_at`, `modified_at`) VALUES
(6, 'hg', 'ff', 'sss', 'ff@dd.com', 'fff', 'fff', 'Male', '11 (1).jpg', 455, '2017-05-20 18:33:33', '2017-05-28 16:24:00'),
(7, 'jalal', 'ahamad', 'jalal2', 'jalal@gmail.com', '123456', '123456', 'male', 'SEID-168280.jpg', 1982545, '2017-05-21 00:03:24', '2017-05-21 00:03:24'),
(8, 'jalal2', 'ahamad2', 'jalal7', 'jalal7@gmail.com', '123', '123', 'Male', 'Penguins.jpg', 123456, '2017-05-28 16:05:43', '2017-05-28 16:16:53'),
(9, 'arif', 'hossain', 'saad', 'saad@gmail.com', '123', '123', 'Male', '11 (3).jpg', 1813, '2017-05-28 16:08:57', '2017-05-28 16:24:15'),
(10, 'Azad ', 'Hossain', 'roni', 'azad@gmail.com', '123456', '123456', 'male', 'jalalvai.jpg', 256412, '2017-05-28 16:39:16', '2017-05-28 16:39:16'),
(11, 'kamal', 'Hossain', 'kamal', 'kamal@gmail.com', '123', '', 'male', 'Lighthouse.jpg', 213654, '2017-05-28 16:43:32', '2017-05-28 16:43:32');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(11) NOT NULL,
  `patients_id` int(11) NOT NULL,
  `patients_name` varchar(255) NOT NULL,
  `report_title` varchar(255) NOT NULL,
  `report_file` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `patients_id`, `patients_name`, `report_title`, `report_file`, `created_at`, `modified_at`) VALUES
(1, 2, 'Md.Montasir', 'blood test', 'freshpsdsfiles25.jpg', '2017-05-20 13:18:07', '2017-05-20 13:18:07'),
(2, 4, 'Md.Arifur Rahman', 'blood test', 'Capture1.JPG', '2017-05-20 13:19:17', '2017-05-20 13:19:17'),
(3, 11, 'sdfds', 'eeee', 'profile.PNG', '2017-05-20 19:06:56', '2017-05-20 19:06:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `username`, `password`) VALUES
(1, 'Jalal Ahamad', 'jalal@gmail.com', 'jalal', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3'),
(2, 'Engineer Jalal', 'jalalvai@gmail.com', 'jalalvai', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
