<?php 

// Start Session
session_start();
// check user login
if(!empty($_SESSION['id']))
{
    header("Location:patients/appointments-reserved-empty.php");
}

// Database connection
require __DIR__ . '/lib/connect.php';
$db = DB();

// Application library ( with CmsAdminLib class )
require __DIR__ . '/lib/library.php';
$app = new CmsAdminLib();

$login_error_message = '';

// check Login request
if (!empty($_POST['patientLogin'])) {

    $username = trim($_POST['username']);
    $password = trim($_POST['password']);

    if ($username == "") {
        $login_error_message = 'Username field is required!';
    } else if ($password == "") {
        $login_error_message = 'Password field is required!';
    } else {
        $id = $app->Login($username, $password); // check user login
        if($id > 0)
        {
            $_SESSION['id'] = $id; // Set Session
            header("Location:patients/my-account.php "); // Redirect user to the dashboard
        }
        else
        {
            $login_error_message = 'Invalid login details!';
        }
    }
}

include 'header.php';  
?>

    <body class="panel-access">

        <div id="layout">
             <!--Login-->
                <div class="login">
                    <div class="container">
                        <div class="login-form">
                            <div class="data-form">
                                <span class="back-to-homepage">
                                    <a class="btn btn-green btn-xsmall" href="index.php"><i class="fa fa-home"></i> Back to Homepage</a>
                                </span>
                                <!--Logo-->
                                <a href="index.php" class="logo reg-logo"><img src="images/login-logo.png" alt="logo" class="img-responsive"></a>
                                <!--Logo-->

                                <?php
                                    if ($login_error_message != "") {
                                    echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $login_error_message . '</div>';
                                    }
                                ?>
                                
                                <!--Form-->
                                <form class="form-login"  role="form" action="login.php" method="post">

                                    <div class="icon-data">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <input type="text" placeholder="User Name or Email" name="username">

                                    <div class="icon-data">
                                        <i class="fa fa-key"></i>
                                    </div>
                                    <input type="password" placeholder="Password" name="password">

                                    <button type="submit" name="patientLogin" value="submit" class="btn btn-default" role="button">Login</button>

                                   
                                </form>
                                <!--Form-->

                                <a href="register.php" class="btn btn-red register" role="button">Create a new account</a>
                                <span class="help">
                                    <a href="forgot-pass.php">Forgot Password?</a>
                                    <a href="help.php" class="help-link">Help?</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

        <?php include 'footer.php';  ?>
        