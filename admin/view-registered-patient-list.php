<?php
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `patients`";
// var_dump($query);
include 'header.php'; 
?>

<div id="wrapper">
    <!-- Navigation -->
    <?php include 'navigation.php'; ?>

    <div id="page-wrapper">
        <div class="container-fluid margin-top">
             
            <div class="table-responsive">     
            <table class="table table-hover margin-top">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Fast Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Username</th>
                  <th>Gender</th>
                  <th>Contact No:</th>
                  <th>Picture</th>
                  <th>Created At</th>
                  <th>Modified At</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                   <!-- <tr>
                    <td>1</td>
                    <td>Jalal</td>
                    <td>NEPHROLOGY</td>
                    <td>Health Care</td>
                    <td>10-5-17</td>
                    <td>10am to 11am</td>
                    <td>220</td>
                    <td>test</td>
                    <td>jalal.jpg</td>
                    <td>5-15-17</td>
                    <td>5-15-17</td>
                    <td>
                      <a href="#" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> Show</a>
                      <a href="#" class="btn btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                      <a href="#" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Delete</a>
                    </td>
                  </tr>  -->
             
                    <?php 
                  foreach($db->query($query) as $patients) { ?>
                  <tr>
                    <td><?php echo $patients['id'];?></td>
                    <td><?php echo $patients['first_name'];?></td>
                    <td><?php echo $patients['last_name'];?></td>
                    <td><?php echo $patients['email'];?></td>
                    <td><?php echo $patients['username'];?></td>
                    <td><?php echo $patients['gender'];?></td>
                    <td><?php echo $patients['contact_no'];?></td>
                    <td><img width="80px" src="images/<?php echo $patients["image"]; ?>"/> </td>
                    <td><?php echo date("d/m/Y", strtotime($patients['created_at']));?></td>
                    <td><?php echo date("d/m/Y", strtotime($patients['modified_at']));?></td>
                    <td>
                      <a href="view-single-patient.php?id=<?php echo $patients['id'];?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> Show</a>
                      <a href="edit-patient.php?id=<?php echo $patients['id'];?>" class="btn btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                      <a href="delete-patient.php?id=<?php echo $patients['id'];?>" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Delete</a>
                    </td>
                  </tr>
                <?php }
                ?>
                
              </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>