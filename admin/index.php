<?php

// Start Session
session_start();

// Database connection
require __DIR__ . '/database.php';
$db = DB();

// Application library ( with DemoLib class )
require __DIR__ . '/lib/library.php';
$app = new CmsAdminLib();

$login_error_message = '';
$register_error_message = '';

// check Login request
if (!empty($_POST['btnLogin'])) {

    $username = trim($_POST['username']);
    $password = trim($_POST['password']);

    if ($username == "") {
        $login_error_message = 'Username field is required!';
    } else if ($password == "") {
        $login_error_message = 'Password field is required!';
    } else {
        $user_id = $app->Login($username, $password); // check user login
        if($user_id > 0)
        {
            $_SESSION['user_id'] = $user_id; // Set Session
            header("Location: dashboard.php"); // Redirect user to the dashboard.php
        }
        else
        {
            $login_error_message = 'Invalid login details!';
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Login</title>
    <link href="css/login.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <body class="panel-access">
        <div id="layout">
             <!--Login-->
            <div class="login">
                <div class="container">
                    <div class="login-form">
                        <div class="data-form">
                            <a href="index.php" class="logo reg-logo"><img src="images/admin-logo.png" alt="logo" class="img-responsive"></a>

                            <?php
                            if ($login_error_message != "") {
                            echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $login_error_message . '</div>';
                            }
                            ?>

                            <form class="form-login" role="form" method="post" action="index.php">
                                <div class="icon-data">
                                    <i class="fa fa-user"></i>
                                </div>
                                <input type="text" placeholder="User Name" name="username">
                                <div class="icon-data">
                                    <i class="fa fa-key"></i>
                                </div>
                                <input type="password" placeholder="Password" name="password">
                                <!-- <button type="submit" name="btnLogin" class="btn btn-default" role="button">Login</button> -->
                                <input type="submit" name="btnLogin" class="btn btn-primary" value="Login"/>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
        