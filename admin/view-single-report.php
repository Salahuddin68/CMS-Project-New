<?php
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `reports` WHERE id =".$_GET['id'];
// var_dump($query);
foreach($db->query($query) as $row) {
  $reports = $row;
}
include 'header.php'; 
?>



<div id="wrapper">
    <!-- Navigation -->
    <?php include 'navigation.php'; ?>

    <div id="page-wrapper">
        <div class="container-fluid" style="margin-top: 20px;">
        <a href="add-report.php" class="btn btn-success"><i class="fa fa-plus"></i> Add New Report</a>
        <a href="view-report-list.php" class="btn btn-info"><i class="fa fa-eye"></i> View All Report</a>      
          

            <div class="row" style="margin-top: 40px;">
              <div class="col-md-4">
                <img  src="images/<?php echo $reports["report_file"]; ?>" alt="" class="img-thumbnail profile-image">
              </div>
              <div class="col-md-8">
                
                <div class="pro-desc">
                  <h3><strong><?php echo $reports['patients_name'];?></strong></h3>

                  <table class="table table-striped">
                    <tbody>
                     <tr>
                      <td><strong>Patient Id</strong></td>
                      <td><?php echo $reports['patients_id'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Report Title</strong></td>
                      <td><?php echo $reports['report_title'];?></a></td>
                    </tr>
                    
                    <tr>
                      <td><strong>Created Date</strong></td>
                      <td><?php echo date("d/m/Y", strtotime($reports['created_at']));?></td>
                    </tr>
                    <tr>
                      <td><strong>Modification Date</strong></td>
                      <td><?php echo date("d/m/Y", strtotime($reports['modified_at']));?></td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </div>
          </div>

        </div>
    </div>
</div>

<?php include 'footer.php'; ?>
